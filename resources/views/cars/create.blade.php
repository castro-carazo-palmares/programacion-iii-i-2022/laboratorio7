@extends('layout')

@section('title', "Insertar Carro")

@section('content')
    <div class="contenedor">
        <h1>Insertar un nuevo carro</h1>

        <div class="form">
            <form action="{{ route('cars.store') }}" method="POST">
                @csrf

                <label for="marca">Marca</label>
                <input type="text" name="marca" required>

                <label for="modelo">Modelo</label>
                <input type="text" name="modelo" required>

                <label for="placa">Placa</label>
                <input type="text" name="placa" required>

                <label for="color">Color</label>
                <input type="text" name="color" required>

                <label for="cilindraje">Cilindraje</label>
                <input type="number" name="cilindraje" required>

                <label for="anio">Año</label>
                <input type="number" name="anio" required>

                <div class="botones">
                    <input class="link create-link" type="submit" value="Insertar">
                    <a href="{{ route('cars.index') }}" class="link regresar-link">Regresar</a>
                </div>
            </form>
        </div>
    </div>
@endsection
