@extends('layout')

@section('title', "Editar Carro")

@section('content')
    <div class="contenedor">
        <h1>Editar carro</h1>

        <div class="form">
            <form action="{{ route('cars.update', $car) }}" method="POST">
                @csrf
                @method('PATCH')

                <label for="id">ID</label>
                <input type="number" name="id" value="{{ $car->id }}" readonly>

                <label for="marca">Marca</label>
                <input type="text" name="marca" value="{{ $car->marca }}" required>

                <label for="modelo">Modelo</label>
                <input type="text" name="modelo" value="{{ $car->modelo }}" required>

                <label for="placa">Placa</label>
                <input type="text" name="placa" value="{{ $car->placa }}" required>

                <label for="color">Color</label>
                <input type="text" name="color" value="{{ $car->color }}" required>

                <label for="cilindraje">Cilindraje</label>
                <input type="number" name="cilindraje" value="{{ $car->cilindraje }}" required>

                <label for="anio">Año</label>
                <input type="number" name="anio" value="{{ $car->anio }}" required>

                <div class="botones">
                    <input class="link edit-link" type="submit" value="Actualizar">
                    <a href="{{ route('cars.show', $car) }}" class="link regresar-link">Regresar</a>
                </div>
            </form>
        </div>
    </div>
@endsection
