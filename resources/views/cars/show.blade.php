@extends('layout')

@section('title', "Mostrar Carro")

@section('content')
    <div class="contenedor">
        <h1>Placa: {{ $car->placa }}</h1>

        <h2>ID: {{ $car->id }}</h2>
        <h2>Marca: {{ $car->marca }}</h2>
        <h2>Modelo: {{ $car->modelo }}</h2>
        <h2>Color: {{ $car->color }}</h2>
        <h2>Cilindraje: {{ $car->cilindraje }} cc</h2>
        <h2>Año: {{ $car->anio }}</h2>

        <div class="botones">
            <a class="link regresar-link" href="{{ route('cars.index') }}">Regresar</a>
            <a class="link edit-link" href="{{ route('cars.edit', $car) }}">Editar</a>
            <form action="{{ route('cars.destroy', $car) }}" method="POST">
                @csrf
                @method('DELETE')
                <input class="link delete-link" type="submit" value="Eliminar">
            </form>
        </div>
    </div>
@endsection
