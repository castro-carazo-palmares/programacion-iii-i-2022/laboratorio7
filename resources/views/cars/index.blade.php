@extends('layout')

@section('title', "Carros")

@section('content')
    <div class="contenedor">
        <h1>Carros</h1>

        <a class="link insert-link" href="{{ route('cars.create') }}">Insertar Carro</a>

        <div class="tarjetas">
            @forelse($cars as $car)
                <div class="tarjeta">
                    <a href="{{ route('cars.show', $car) }}">
                        <h3>{{ $car->placa }}</h3>
                    </a>
                </div>
            @empty
                <div class="tarjeta">
                    <h3>No hay carros para mostrar</h3>
                </div>
            @endforelse
        </div>
    </div>
@endsection
