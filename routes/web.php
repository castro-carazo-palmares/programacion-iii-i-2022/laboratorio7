<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\CarController@index')->name('cars.index');
Route::get('/crear', 'App\Http\Controllers\CarController@create')->name('cars.create');
Route::post('/', 'App\Http\Controllers\CarController@store')->name('cars.store');
Route::get('/{car}', 'App\Http\Controllers\CarController@show')->name('cars.show');
Route::get('/{car}/editar', 'App\Http\Controllers\CarController@edit')->name('cars.edit');
Route::patch('/{car}', 'App\Http\Controllers\CarController@update')->name('cars.update');
Route::delete('/{car}', 'App\Http\Controllers\CarController@destroy')->name('cars.destroy');
